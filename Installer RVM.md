# Installer RVM

RVM est un gestionnaire de version de Ruby. Il permet de passer rapidement d'une version à l'autre de Ruby sans désintaller et réinstaller Ruby.

## Installer RVM

Commencer par installer les dépendances de Ruby:

`$ sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev`


Installer la clé de sécurité nécéssaire au téléchargement de rvm via la ligne de commande :

`$ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3`


Télécharger RVM, et installer Ruby:

`$ \curl -sSL https://get.rvm.io | bash -s stable --ruby`


Réactualiser les scripts du shell, pour que les modifictations soient prises en compte

`$ source ~/.rvm/scripts/rvm`


**Une fois toute ces étapes terminées, il faut relancer la console** : fermer puis en rouvrir une nouvelle


`$ bash -s stable`

Rajouter ces lignes au fichier ~/.bashrc file (facultatif?).

`[[ -s "$HOME/.rvm/scripts/rvm" ]] && . "$HOME/.rvm/scripts/rvm"  # This loads RVM`

Et relancer la console (ou fermer le terminal et le réouvrir).

`$ source ~/.bashrc`

Vérifier que Ruby est bien trouvé par la console

`$ type rvm`

Si vous voyez une réponse comme:

`$ type rvm`
`rvm is a shell function`

félicitation !! rvm est correctement installé sur l'ordinateur.

Si la console retourne un chemin vers un dossier:

1. Clic droit sur la console
2. Profil > Préférences ou Préférences onglet Profil (terminator)
3. Onglet Commande
4. Cocher "Lancer la console en tant que shell de connexion" (Run command as login shell)
et

**Relancer une nouvelle fois la console**

`$ source ~/.bashrc`

On peut voir les versions de ruby avec la commande :

`rvm list known`

On peut installer une version stable et récente :

`rvm install 2.3.0`

ou pour sur la dernière version stable :

`$ rvm get stable`

### Une nouvelle version des gems est à mettre en place avec une mise à jour :

`$ gem update --system`

`$ gem update`

### Installation de Gems plus rapide (facultatif mais gain de temps)

Par defaut, en installant les gems, les dossiers de documentation sont installés. Les développeurs ne les utilisent pas toujours (ils la cherche sur le web à la place). Les installer prends du temps, pour cela souvent les développeurs installent les gems sans la documentation.

Voici comment installer plus vite les gems sans installer les fichiers de documentation:

`$ echo "gem: --no-ri --no-rdoc" >> ~/.gemrc`

Ceci ajoute à la ligne gem: --no-ri --no-rdoc au fichier caché .gemrc dans le dossier home.


## Installer RubyOnRails et ses dépendances grâce aux Gems Ruby

Dans le terminal :

`$ gem install rails`

Ensuite, installer la librairie dont rails a besoin pour tourner sur Linux
Pour Ubuntu/Debian :

`$ sudo apt-get install libpq-dev`

On a besoin aussi de bundler qui est un gestionnaire de librairie indépendant (un peu comme un apt de pour rails)

`$ gem install bundler`

Une des librairies aidant à compiler et qui est souvent une dépendance pour d'autres librairies et un peu longue à s'installer, du coup c'est mieux de l'installer dans les gems globales gemset pour gagner du temps:

`$ gem install nokogiri`


## Installer Node.js

Rails depuis sa version 3.1 a besoin des runtime Javascripts de Node.js aussi, installer node.js :

`$ sudo apt-get install nodejs`

__Sources :__
[RVM](https://rvm.io/rvm/install),

[Rails App Project](https://railsapps.github.io/installrubyonrails-ubuntu.html)
